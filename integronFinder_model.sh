#!/usr/bin/env bash
#
########################################################################
## Parametros del QSUB ##
########################################################################
#PBS -V
#PBS -N integronFind
#PBS -l nodes=1:ppn=1
#PBS -M luchamosa@fmed.uba.ar
#PBS -m abe
cd $PBS_O_WORKDIR
########################################################################

## Ruta a la carpeta de trabajo
########################################################################
WORKDIR="/home/luchamosa/contigs.../";
########################################################################


########################################################################
## crear array para contener los contigs
########################################################################
arrfasta=();

## entrar a carpeta de los fasta 
cd "$WORKDIR";

## fill the array with the fasta's
for i in *.fasta; do arrfasta+=( "$i" );done

#sort the array alphanumerically by contig number
arrfasta=($(for each in ${arrfasta[@]}; do echo $each; done | sort -k2 -t# -n));
########################################################################


########################################################################
## carpeta donde van a ir los outputs
########################################################################
output="$WORKDIR/output";
########################################################################


########################################################################
## ejecutar integronfinder
########################################################################
count=0;
while [ $count -lt "${#arrfasta[@]}" ];
do integron_finder  --local_max --linear ${arrfasta[ $count ]};
 (( count++ ));
done
########################################################################