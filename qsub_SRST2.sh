#!/bin/bash
##
#PBS -V
############################################################
## MODELO DE BASH SCRIPT PARA EJECUTAR UN COMANDO MEDIANTE #
##        EL SISTEMA DE COLAS (QSUB) DEL SERVIDOR IMPAM    #
##                                                         #
##               glichtenstein@fmed.uba.ar                 #
############################################################
#
#These commands set up the Grid Environment for your job:
#
##################################################
## 1) Poner un nombre para identificar tu script #
##################################################
#
#PBS -N "SRST2"
#
############################################################################
## 2) Configurar los recursos que quiero reservar en el cluster            #
############################################################################
#
#PBS -l nodes=1:ppn=1
#
############################################################################
## 3) notificaciónes del qsub via email al comenzar y terminar el proceso: #
############################################################################
#PBS -M mmasso@fmed.uba.ar
#PBS -m abe 
#########################################
#
cd $PBS_O_WORKDIR
#
############################################################################
## 4) Debajo de este título, va el comando que desea ejecutar,             #
## tal como lo escribiría en la línea de comandos                          #
############################################################################
# descender a la carpeta de trabajo:
cd /home/mmasso/Desktop/CURSO_CELFI/

# exportar al path la version de samtools a utilizar por SRST2
export SRST2_SAMTOOLS=/share/apps/miniconda3/bin/samtools

# comando para ejecutar SRST2 con PE reads:
# nota: cree una carpeta input y otra de output porque se generan muchos archivos
# nota1= utilizo 4 threads ( 4 CPUs) para que sea más rápido el proceso
srst2 \
--input_pe "input/8157_S5_L001_R1_001.fastq" "input/8157_S5_L001_R2_001.fastq" \
--gene_db "input/ARGannot.r1.fasta" \
--threads 4 \
--log \
--output "output/8157_output"