#!/bin/bash
##
#PBS -V
############################################################
## MODELO DE BASH SCRIPT PARA EJECUTAR UN COMANDO MEDIANTE #
##        EL SISTEMA DE COLAS (QSUB) DEL SERVIDOR IMPAM    #
##                                                         #
##               glichtenstein@fmed.uba.ar                 #
############################################################
#
#These commands set up the Grid Environment for your job:
#
##################################################
## 1) Poner un nombre para identificar tu script #
##################################################
#
#PBS -N cut+trimm
#
############################################################################
## 2) Configurar los recursos que quiero reservar en el cluster            #
############################################################################
# recorda que la carpeta BigData esta en impam 1
#
#PBS -l nodes=impam.fmed.uba.ar:ppn=1
#
############################################################################
## 3) notificaciónes del qsub via email al comenzar y terminar el proceso: #
############################################################################
#PBS -M glichtenstein@fmed.uba.ar
#PBS -m abe
#########################################
#
cd $PBS_O_WORKDIR
#
############################################################################
## 4) Debajo de este título, va el comando que desea ejecutar,             #
## tal como lo escribiría en la línea de comandos                          #
############################################################################
# Un ejemplo de la library 2018 de Marce Cucher 
############################################################################
#
cd /BigData/marce/stan_2018_miRNAs/01-reads
#
############################################################################
# LIBRARY 1 (primera especie) 1#1
############################################################################
# First: cut adaptors
i=1;
raw=0;
cutadapt -m 20 -b file:adapters/set_de_adaptadores_4 -B file:adapters/set_de_adaptadores_4 \
-o 3-preprocessed/24246_1#1_iteration$i.trimm_and_cut_1P.fastq \
-p 3-preprocessed/24246_1#1_iteration$i.trimm_and_cut_2P.fastq \
3-preprocessed/24246_1#1_iteration$raw.trimm_and_cut_1P.fastq \
3-preprocessed/24246_1#1_iteration$raw.trimm_and_cut_2P.fastq && \
#
# Second: trimm low-quality with sliding window above 20 PHRED
let i=2;
let raw=1;
java -jar /share/apps/Trimmomatic-0.36/trimmomatic-0.36.jar PE \
-threads 6 \
-phred33 \
-trimlog "/BigData/marce/stan_2018_miRNAs/01-reads/3-preprocessed/24246_1#.trimmomatic.log" \
3-preprocessed/24246_1#1_iteration$raw.trimm_and_cut_1P.fastq  3-preprocessed/24246_1#1_iteration$raw.trimm_and_cut_2P.fastq \
-baseout 3-preprocessed/24246_1#1_iteration$i.trimm_and_cut.fastq \
ILLUMINACLIP:/BigData/marce/stan_2018_miRNAs/01-reads/adapters/set_de_adaptadores_4:2:10:10 SLIDINGWINDOW:4:20 MINLEN:20 && \
#
#############################################################################
# LIBRARY 2 (segunda especie) 1#2
#############################################################################
# First: cut adaptors
let i=1;
let raw=0;
cutadapt -m 20 -b file:adapters/set_de_adaptadores_4 -B file:adapters/set_de_adaptadores_4 \
-o 3-preprocessed/24246_1#2_iteration$i.trimm_and_cut_1P.fastq \
-p 3-preprocessed/24246_1#2_iteration$i.trimm_and_cut_2P.fastq \
3-preprocessed/24246_1#2_iteration$raw.trimm_and_cut_1P.fastq \
3-preprocessed/24246_1#2_iteration$raw.trimm_and_cut_2P.fastq && \
#
# Second: trimm with sliding window above 20 PHRED
let i=2;
let raw=1;
java -jar /share/apps/Trimmomatic-0.36/trimmomatic-0.36.jar PE \
-threads 6 \
-phred33 \
-trimlog "/BigData/marce/stan_2018_miRNAs/01-reads/3-preprocessed/24246_1#.trimmomatic.log" \
3-preprocessed/24246_1#2_iteration$raw.trimm_and_cut_1P.fastq  3-preprocessed/24246_1#2_iteration$raw.trimm_and_cut_2P.fastq \
-baseout 2-preprocessed/24246_1#2_iteration$i.trimm_and_cut.fastq \
ILLUMINACLIP:/BigData/marce/stan_2018_miRNAs/01-reads/adapters/set_de_adaptadores_4:2:10:10 SLIDINGWINDOW:4:20 MINLEN:20

# nota: los nombres de los archivos habria que simplificarlos un poco:
# 2_iteration$raw.trimm_and_cut_ es muy largo...