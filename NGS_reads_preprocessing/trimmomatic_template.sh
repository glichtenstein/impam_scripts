#!/usr/bin/env bash

# Ejemplo de trimmomatic para recortar bases de baja calidad de una librer�a paired-end de illumina.
# Son dos archivos fastq (Forward y Reverse):
# 1) HU85A_S49_L001_R1_001.fastq
# 2) HU85A_S49_L001_R2_001.fastq
# basein es un parametro que trata de detectar ambos archivos usando el nombre del archivo como base para esto.
# se recomienda leer el manual del trimmomatic y usar el fastqc para ir visualizando los resultados.
# headcrop recorta nt del 5'sin importar la calidad, ojo al usarlo, complementar con fastqc.
# aqui usamos MAXINFO, como alternativa o setear m�s parametros usar SLIDINGWINDOW

java -jar /share/apps/Trimmomatic-0.36/trimmomatic-0.36.jar PE \
-threads 6 \
-phred33 \
-trimlog "HU85A_S49_L001.trimmlog" \
-basein "HU85A_S49_L001_R1_001.fastq" \
-baseout "HU85A_S49_L001_R1_001.filtered_40_0.9_headcrop_15.fastq" \
MAXINFO:40:0.9 HEADCROP:15 MINLEN:36
