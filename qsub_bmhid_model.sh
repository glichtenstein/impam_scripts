#!/bin/bash
##
#PBS -V
############################################################
## MODELO DE BASH SCRIPT PARA EJECUTAR UN COMANDO MEDIANTE #
##        EL SISTEMA DE COLAS DEL SERVIDOR O QSUB          #
##                                                         #
##               glichtenstein@fmed.uba.ar                 #
############################################################
#
#These commands set up the Grid Environment for your job:
#
##################################################
## 1) Poner un nombre para identificar tu script #
##################################################
#
#PBS -N ProcesoX
#
############################################################################
## 2) Configurar los recursos que quiero reservar en el cluster            #
############################################################################
#
#PBS -l nodes=1:ppn=1
#
############################################################################
## 3) notificaciónes del qsub por email al comenzar y terminar el proceso: #
############################################################################
#PBS -M glichtenstein@fmed.uba.ar
#PBS -m abe
################################################
## 4) Posicionarse en el directorio de trabajo #
################################################
#
cd $PBS_O_WORKDIR
#
################################################
## 5) seleccionar la cola (queue) para bmhid   #
################################################
#
#PBS -q bmhid
#
#################################################################
## 6) Debajo de este título, va el comando que desea ejecutar,  #
##    tal como lo escribiría en la línea de comandos            #
#################################################################
date # este es un comando de ejemplo para testear el script, borrar para escribir el suyo
sleep 10
date
