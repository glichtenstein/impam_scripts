#!/bin/bash
##
#PBS -V
#
#:::::::::::::::::::::::::::::::::::::::::::::
# Build a bowtie reference from a fasta file. 
# for example a genome.fasta
#:::::::::::::::::::::::::::::::::::::::::::::
#
############################################################
## MODELO DE BASH SCRIPT PARA EJECUTAR UN COMANDO MEDIANTE #
##        EL SISTEMA DE COLAS (QSUB) DEL SERVIDOR IMPAM    #
##                                                         #
##               glichtenstein@fmed.uba.ar                 #
############################################################
#
#These commands set up the Grid Environment for your job:
#
##################################################
## 1) Poner un nombre para identificar tu script #
##################################################
#
#PBS -N "MiProcesoX"
#
############################################################################
## 2) Configurar los recursos que quiero reservar en el cluster            #
############################################################################
#
#PBS -l nodes=1:ppn=1
#
############################################################################
## 3) notificaciónes del qsub via email al comenzar y terminar el proceso: #
############################################################################
#PBS -M usuario@fmed.uba.ar
#PBS -m abe 
#########################################
#
cd $PBS_O_WORKDIR
#
############################################################################
## 4) Debajo de este título, va el comando que desea ejecutar,             #
## tal como lo escribiría en la línea de comandos                          #
############################################################################
bowtie2-build -f mygenome.fasta mygenome