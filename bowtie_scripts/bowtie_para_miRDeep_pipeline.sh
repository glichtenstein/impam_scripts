#!/usr/bin/env bash

#---------------------------#
#variables:
#---------------------------#

# indexed reference genome 
ebwt=;

# library 1 (es el output del mapper)
fasta1=;
output1=;

# library 2  (es el output del mapper)
fasta2=;
output2=;

#---------------------------#
#comando para 1#1
#---------------------------#
bowtie -v 2 --sam --al --best --time --threads 12 $ebwt -f $fasta1 $output && \

#---------------------------#
#comando para 1#2
#---------------------------#
bowtie -v 2 --sam --al --best --time --threads 12 $ebwt -f $fasta2 $output
