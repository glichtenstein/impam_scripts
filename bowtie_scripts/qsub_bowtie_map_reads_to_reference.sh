#!/bin/bash
##
#PBS -V
############################################################
## MODELO DE BASH SCRIPT PARA EJECUTAR UN COMANDO MEDIANTE #
##        EL SISTEMA DE COLAS (QSUB) DEL SERVIDOR IMPAM    #
##                                                         #
##               glichtenstein@fmed.uba.ar                 #
############################################################
#
#These commands set up the Grid Environment for your job:
#
##################################################
## 1) Poner un nombre para identificar tu script #
##################################################
#
#PBS -N "MiProcesoX"
#
############################################################################
## 2) Configurar los recursos que quiero reservar en el cluster            #
############################################################################
#
#PBS -l nodes=1:ppn=1
#
############################################################################
## 3) notificaciónes del qsub via email al comenzar y terminar el proceso: #
############################################################################
#PBS -M usuario@fmed.uba.ar
#PBS -m abe 
#########################################
#
cd $PBS_O_WORKDIR
#
############################################################################
## 4) Debajo de este título, va el comando que desea ejecutar,             #
## tal como lo escribiría en la línea de comandos                          #
############################################################################
# A saber:
# Paired-end:
#  -I/--minins <int>  minimum fragment length (0)
#  -X/--maxins <int>  maximum fragment length (500)

# Abajo esta el comando de Bowtie2 para mapear un set de paired-end reads fastq 
# a un genoma de referencia (contigs) en formato fasta que fue indexado 
# previamente con bowtie_build

bowtie2 \
-p 6 \
-x mygenome \
-1 pair1.trimmed.fastq \
-2 pair2.trimmed.fastq \
-U unpaired.trimmed.fastq \
--sensitive \
-X 300 \
-I 100 \
| samtools view -bS - > output.bam
