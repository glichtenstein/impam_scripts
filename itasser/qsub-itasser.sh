#!/bin/bash
##
#PBS -V
############################################################
## MODELO DE BASH SCRIPT PARA EJECUTAR UN COMANDO MEDIANTE #
##        EL SISTEMA DE COLAS (QSUB) DEL SERVIDOR IMPAM    #
##                                                         #
##               glichtenstein@fmed.uba.ar                 #
############################################################
#
#These commands set up the Grid Environment for your job:
#
##################################################
## 1) Poner un nombre para identificar tu script #
##################################################
#
#PBS -N itasser
#
############################################################################
## 2) Configurar los recursos que quiero reservar en el cluster            #
############################################################################
#
#PBS -l nodes=impam2+impam.fmed.uba.ar
#PBS -q bmhid
############################################################################
## 3) notificaciónes del qsub via email al comenzar y terminar el proceso: #
############################################################################
#PBS -M glichtenstein@fmed.uba.ar
#PBS -m abe
#########################################
#
cd $PBS_O_WORKDIR
#
############################################################################
## 4) Debajo de este título, va el comando que desea ejecutar,             #
## tal como lo escribiría en la línea de comandos                          #
############################################################################

#-------------------------------------------------------------------------#
# I-tasser test basico #
#runI-TASSER.pl \
#-runstyle parallel \
#-libdir /share/apps/databases/ITLIB/ \
#-seqname /share/apps/I-TASSER5.1/example/seq.fasta \
#-datadir /home/glichtenstein/I-TASSER5.0/example/
#-------------------------------------------------------------------------#
# I-tasser parallel PBS and open MPI test#
mpiexec --hostfile /home/glichtenstein/I-TASSER5.0/example_21feb2018/my-hosts -np 18 \
/share/apps/I-TASSER5.1/I-TASSERmod/runI-TASSER.pl \
-runstyle parallel \
-java_home /usr \
-libdir /share/apps/databases/ITLIB \
-datadir /home/glichtenstein/I-TASSER5.0/example_21feb2018/ \
-seqname seq.fasta \
-outdir /home/glichtenstein/I-TASSER5.0/example_21feb2018/ \
-light true \
-hours 6 \
-LBS true \
-EC true \
-GO true
#-------------------------------------------------------------------------#
# make html of results
#/share/apps/I-TASSER5.1/file2html/file2html.py /home/glichtenstein/I-TASSER5.0/example3/
#-pkgdir /share/apps \
