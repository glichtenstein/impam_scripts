#!/usr/bin/bash

# an example command of how to make a Blast DB for the swissprot fasta file
makeblastdb \
-in "swissprot.fa" \
-input_type "fasta" \
-parse_seqids \
-dbtype prot \
-title "SWISS-PROT | curated protein sequence database" \
-out TrEMBL