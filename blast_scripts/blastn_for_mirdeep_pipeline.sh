#!/usr/bin/env bash
#################################################################################################################################
# VARIABLES
################################################################################################################################
# Queries
QUERY1="/BigData/marce/sanger_RNAseq/04-tRNA_search/01-bowtie/24246_1#1_preprocessed+mapper_vs_emul_genome.aligned.fasta";
QUERY2="/BigData/marce/sanger_RNAseq/04-tRNA_search/01-bowtie/24246_1#2_preprocessed+mapper_vs_emul_genome.aligned.fasta";

# DataBase
DATA_BASE="/BigData/marce/sanger_RNAseq/02-reference_seqs/tRNAs/cds_rrnas_trnas_lnc_cestodes.v3.fa";

# Outputs:
OUTPUT1="/BigData/marce/sanger_RNAseq/04-tRNA_search/02-blastn/24246_1#1_preprocessed+mapper_vs_emul_genome.aligned.blast.tab";
OUTPUT2="/BigData/marce/sanger_RNAseq/04-tRNA_search/02-blastn/24246_1#2_preprocessed+mapper_vs_emul_genome.aligned.blast.tab";
################################################################################################################################
#
#
################################################################################################################################
# BLAST - iterative loop on the 2 queries:
################################################################################################################################
for i in {$QUERY1,$QUERY2};

do

	blastn \
	-task "blastn" \
	-query "$i" \
	-db "$DATA_BASE" \
	-out "$OUTPUT1" \
	-outfmt "6 qseqid sseqid pident qlen length mismatch gapopen qstart qend sstart send slen evalue" \
	-max_target_seqs "1" \
	-word_size "11" \
	-evalue "0.01" \
	-dust "yes" \
	-gapopen "5" \
	-gapextend "2" \
	-penalty "-2" \
	-reward "1" \
	-best_hit_overhang "0.1" \
	-best_hit_score_edge "0.1" \
	-num_threads "12";

	if [ $OUTPUT1 != $OUTPUT2 ]
	then
	export OUTPUT1="$OUTPUT2"
	else
	exit;
	fi;

done
################################################################################################################################