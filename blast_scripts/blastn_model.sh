#!/bin/bash
##
#PBS -V
########################################################################
## MODELO DE BASH SCRIPT PARA EJECUTAR UN BLAST MEDIANTE 
##        EL SISTEMA DE COLAS (QSUB) DEL SERVIDOR IMPAM    
##                                                         
##                  glichtenstein@fmed.uba.ar                 
########################################################################
#
#These PBS commands set up the Grid Environment for your job:
#
########################################################################
## 1) Poner un nombre para identificar tu script #
########################################################################
#
#PBS -N "blastn"
#
########################################################################
## 2) Configurar los recursos a reservar en el cluster
########################################################################
#
#PBS -l nodes=1:ppn=1
#
########################################################################
## 3) notificaciónes de qsub via email 
########################################################################
#PBS -M usuario@fmed.uba.ar
#PBS -m abe 
########################################################################
#
cd $PBS_O_WORKDIR
#
########################################################################
## 4) set up the BLAST 
########################################################################
# Paths to the "Query" and the "Subject Database" to Hit:
QUERY="";
DATA_BASE="";
#
# Path to the resulting "Output" file:
OUTPUT_FILE="";
########################################################################
#
########################################################################
# BLAST parameters:
########################################################################
blastn \
-task "blastn" \
-query "$QUERY" \
-db "$DATA_BASE" \
-out "$OUTPUT_FILE" \
-outfmt "6 qseqid sseqid pident qlen length mismatch gapopen qstart qend sstart send slen evalue" \
-max_target_seqs "1" \
-word_size "11" \
-evalue "1E-10" \
-dust "yes" \
-gapopen "5" \
-gapextend "2" \
-penalty "-2" \
-reward "1" \
-best_hit_overhang "0.1" \
-best_hit_score_edge "0.1" \
-num_threads "2" 

########################################################################
# Referencia de columnas del output en formato tabla (outfmt 6, 7 ó 10):
########################################################################
#~ Options 6, 7 and 10 can be additionally configured to produce
#~ a custom format specified by space delimited format specifiers.
#~ The supported format specifiers for options 6, 7 and 10 are:

#~ qseqid means Query Seq-id
#~ qgi means Query GI
#~ qacc means Query accesion
#~ qaccver means Query accesion.version
#~ qlen means Query sequence length
#~ sseqid means Subject Seq-id
#~ sallseqid means All subject Seq-id(s), separated by a ';'
#~ sgi means Subject GI
#~ sallgi means All subject GIs
#~ sacc means Subject accession
#~ saccver means Subject accession.version
#~ sallacc means All subject accessions
#~ slen means Subject sequence length
#~ qstart means Start of alignment in query
#~ qend means End of alignment in query
#~ sstart means Start of alignment in subject
#~ send means End of alignment in subject
#~ qseq means Aligned part of query sequence
#~ sseq means Aligned part of subject sequence
#~ evalue means Expect value
#~ bitscore means Bit score
#~ score means Raw score
#~ length means Alignment length
#~ pident means Percentage of identical matches
#~ nident means Number of identical matches
#~ mismatch means Number of mismatches
#~ positive means Number of positive-scoring matches
#~ gapopen means Number of gap openings
#~ gaps means Total number of gaps
#~ ppos means Percentage of positive-scoring matches
#~ frames means Query and subject frames separated by a '/'
#~ qframe means Query frame
#~ sframe means Subject frame
#~ btop means Blast traceback operations (BTOP)
#~ staxids means unique Subject Taxonomy ID(s), separated by a ';'
#~ (in numerical order)
#~ sscinames means unique Subject Scientific Name(s), separated by a ';'
#~ scomnames means unique Subject Common Name(s), separated by a ';'
#~ sblastnames means unique Subject Blast Name(s), separated by a ';'
#~ sskingdoms means unique Subject Super Kingdom(s), separated by a ';'
#~ stitle means Subject Title
#~ salltitles means All Subject Title(s), separated by a '<>'
#~ sstrand means Subject Strand
#~ qcovs means Query Coverage Per Subject
#~ qcovhsp means Query Coverage Per HSP
#~ qcovus means Query Coverage Per Unique Subject (blastn only)

#~ When not provided, the default value is:
#~ 'qseqid sseqid pident length mismatch gapopen qstart qend sstart send
#~ evalue bitscore', which is equivalent to the keyword 'std'
########################################################################
